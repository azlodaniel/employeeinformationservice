using Microsoft.AspNetCore.Mvc;
using EmployeeInformationService.Models;
using System;
using System.Collections.Generic;

namespace EmployeeInformationService.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class EmployeeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        [HttpGet]
        public IEnumerable<Employee> GetAllEmployees()
        {
            return _employeeRepository.GetAllEmployees();
        }

        [HttpGet("{id}", Name = "GetEmployee")]
        public IActionResult GetEmployee(Guid id)
        {
            var employee = _employeeRepository.GetEmployee(id);

            return new OkObjectResult(employee);
        }

        [HttpPost]
        public IActionResult AddEmployee([FromBody]Employee employee)
        {
            _employeeRepository.AddEmployee(employee);

            return CreatedAtRoute("GetEmployee", new { id = employee.EmployeeId }, employee);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateEmployee(Employee employee)
        {
            _employeeRepository.UpdateEmployee(employee);

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteEmployee(Guid id)
        {
            _employeeRepository.DeleteEmployee(id);

            return new NoContentResult();
        }
    }
}