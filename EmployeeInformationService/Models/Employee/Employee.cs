﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmployeeInformationService.Models
{
    public class Employee
    {
        public Guid EmployeeId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public decimal Salary { get; set; }
        public string PictureFileName { get; set; }
    }
}
