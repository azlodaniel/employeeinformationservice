﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeInformationService.Models
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly EmployeeContext _context;
        private string newEmployeePicture = "missing.gif";

        public EmployeeRepository(EmployeeContext context)
        {
            _context = context;
        }

        public List<Employee> GetAllEmployees()
        {
            var employee = _context.Employee.ToList();

            return employee;
        }

        public void AddEmployee(Employee employee)
        {
            employee.EmployeeId = Guid.NewGuid();
            employee.PictureFileName = newEmployeePicture;

            _context.Employee.Add(employee);
            _context.SaveChanges();
        }

        public void DeleteEmployee(Guid employeeId)
        {
            var employee = _context.Employee.FirstOrDefault(f => f.EmployeeId == employeeId);

            if (employee != null)
            {
                _context.Employee.Remove(employee);
                _context.SaveChanges();
            }
        }

        public void UpdateEmployee(Employee employee)
        {
            _context.Entry(employee).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public Employee GetEmployee(Guid employeeId)
        {
            var employee = _context.Employee.FirstOrDefault(f => f.EmployeeId == employeeId);

            if (employee != null)
            {
                return employee;
            }

            return employee;
        }
    }
}
