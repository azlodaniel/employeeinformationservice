﻿using System;
using System.Collections.Generic;

namespace EmployeeInformationService.Models
{
    public interface IEmployeeRepository
    {
        List<Employee> GetAllEmployees();
        void AddEmployee(Employee employee);
        void DeleteEmployee(Guid employeeId);
        void UpdateEmployee(Employee employee);
        Employee GetEmployee(Guid employeeId);
    }
}
